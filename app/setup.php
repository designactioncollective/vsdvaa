<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['jquery'], null, true);
    wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css', false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

        /**
     * Block styles
     */
    add_theme_support( 'wp-block-styles' );
    add_theme_support( 'align-wide' );

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
         'footer_navigation' => __('Footer Navigation', 'sage'),
         'footer_navigation-blurb' => __('Footer Navigation Bellow Blurb', 'sage'),
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));

    add_theme_support( 'editor-color-palette', array(
        array(
            'name'  => __( 'Purple', 'sage' ),
            'slug'  => 'purple',
            'color'	=> '#30225c',
        ),
        array(
            'name'  => __( 'Green', 'sage' ),
            'slug'  => 'green',
            'color' => '#cdde00',
        ),
        array(
            'name'  => __( 'Light Green', 'sage' ),
            'slug'  => 'light-green',
            'color' => '#dbe934',
        ),
        array(
            'name'  => __( 'Magenta', 'sage' ),
            'slug'  => 'magenta',
            'color' => '#a50064',
        ),
    ) );
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/*
 * Remove "Posts"
 *
function remove_menu_pages() {
	remove_menu_page('edit.php'); // Posts
}
add_action( 'admin_menu', __NAMESPACE__ . '\\remove_menu_pages' );

add_action( 'admin_bar_menu', __NAMESPACE__ . '\\remove_wp_nodes', 999 );
function remove_wp_nodes() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_node( 'new-post' );
}
*/
/*
 * Remove dashboard metaboxes
 */
function remove_dashboard_meta() {
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    // remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
    // Events Calendar
    remove_meta_box( 'tribe_dashboard_widget', 'dashboard', 'normal');
  }
add_action( 'admin_init', __NAMESPACE__ . '\\remove_dashboard_meta' );

/*
    Add logo to Customizer
*/
add_theme_support( 'custom-logo' );

// Add excerpt to pages
add_post_type_support( 'page', 'excerpt' );

// Add featured image to pages
add_theme_support( 'post-thumbnails', array( 'page' ) );

/**
    = Custom Thumbnails
**/
if ( function_exists( 'add_image_size' ) ) {
        add_image_size( 'small', 230,215, true );
        add_image_size( 'medium-large', 735,675, true );
        add_image_size( 'staff-portrait', 262, 262 );
}

// Full width embeds like Video
add_theme_support( 'responsive-embeds' );
