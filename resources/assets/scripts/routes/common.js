export default {
  init() {
    // JavaScript to be fired on all pages

    // Recolor the header background so it's white and not translucent (But only after scroll)
    // When the user scrolls the page, execute myFunction
    window.onscroll = function() {myFunction()};

    // Get the header
    var header = document.querySelector('.nav-primary');

    // Get the offset position of the navbar

    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
      var lowest_opacity = 0.34;
      // The math and variables used to calculate the ratio below.
      // var highest_opacity = 1;
      // var range = highest_opacity - lowest_opacity;
      // var point_where_header_is_highest_capacity = 372;
      // var ratio = range / point_where_header_is_highest_capacity;
      var ratio = 0.00177419354;

      var opacity = (window.pageYOffset * ratio) + lowest_opacity;
      var color = `rgba(255, 255, 255, ${opacity})`;
      header.style.backgroundColor = color;
    }

    // Load Slimmenu
    !(function(e, n, i, s) {
      'use strict';
      function t(n, i) {
        (this.element = n),
          (this.$elem = e(this.element)),
          (this.options = e.extend(d, i)),
          this.init();
      }
      var l = 'slimmenu',
        a = 0,
        d = {
          resizeWidth: '767',
          initiallyVisible: !1,
          collapserTitle: 'Main Menu',
          animSpeed: 'medium',
          easingEffect: null,
          indentChildren: !1,
          childrenIndenter: '&nbsp;&nbsp;',
          expandIcon: '<i>&#9660;</i>',
          collapseIcon: '<i>&#9650;</i>',
        };
      (t.prototype = {
        init: function() {
          var i,
            s = e(n),
            t = this.options,
            l = this.$elem,
            a =
              '<div class="menu-collapser">' +
              t.collapserTitle +
              '<div class="collapse-button"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div></div>';
          l.before(a),
            (i = l.prev('.menu-collapser')),
            l.on('click', '.sub-toggle', function(n) {
              n.preventDefault(), n.stopPropagation();
              var i = e(this).closest('li');
              e(this).hasClass('expanded')
                ? (e(this)
                    .removeClass('expanded')
                    .html(t.expandIcon),
                  i.find('>ul').slideUp(t.animSpeed, t.easingEffect))
                : (e(this)
                    .addClass('expanded')
                    .html(t.collapseIcon),
                  i.find('>ul').slideDown(t.animSpeed, t.easingEffect));
            }),
            i.on('click', '.collapse-button', function(e) {
              e.preventDefault(), l.slideToggle(t.animSpeed, t.easingEffect);
            }),
            this.resizeMenu(),
            s.on('resize', this.resizeMenu.bind(this)),
            s.trigger('resize');
        },
        resizeMenu: function() {
          var i = this,
            t = e(n),
            l = t.width(),
            d = this.options,
            o = e(this.element),
            h = e('body').find('.menu-collapser');
          n.innerWidth !== s && n.innerWidth > l && (l = n.innerWidth),
            l != a &&
              ((a = l),
              o.find('li').each(function() {
                e(this).has('ul').length &&
                  (e(this)
                    .addClass('has-submenu')
                    .has('.sub-toggle').length
                    ? e(this)
                        .children('.sub-toggle')
                        .html(d.expandIcon)
                    : e(this)
                        .addClass('has-submenu')
                        .append(
                          '<span class="sub-toggle">' + d.expandIcon + '</span>'
                        )),
                  e(this)
                    .children('ul')
                    .hide()
                    .end()
                    .find('.sub-toggle')
                    .removeClass('expanded')
                    .html(d.expandIcon);
              }),
              d.resizeWidth >= l
                ? (d.indentChildren &&
                    o.find('ul').each(function() {
                      var n = e(this).parents('ul').length;
                      e(this)
                        .children('li')
                        .children('a')
                        .has('i').length ||
                        e(this)
                          .children('li')
                          .children('a')
                          .prepend(i.indent(n, d));
                    }),
                  o
                    .addClass('collapsed')
                    .find('li')
                    .has('ul')
                    .off('mouseenter mouseleave'),
                  h.show(),
                  d.initiallyVisible || o.hide())
                : (o
                    .find('li')
                    .has('ul')
                    .on('mouseenter', function() {
                      e(this)
                        .find('>ul')
                        .stop()
                        .slideDown(d.animSpeed, d.easingEffect);
                    })
                    .on('mouseleave', function() {
                      e(this)
                        .find('>ul')
                        .stop()
                        .slideUp(d.animSpeed, d.easingEffect);
                    }),
                  o.find('li > a > i').remove(),
                  o.removeClass('collapsed').show(),
                  h.hide()));
        },
        indent: function(e, n) {
          for (var i = 0, s = ''; e > i; i++) s += n.childrenIndenter;
          return '<i>' + s + '</i> ';
        },
      }),
        (e.fn[l] = function(n) {
          return this.each(function() {
            e.data(this, 'plugin_' + l) ||
              e.data(this, 'plugin_' + l, new t(this, n));
          });
        });
    })(jQuery, window, document);
    jQuery('.slimmenu').slimmenu({
      resizeWidth: '1200',
      collapserTitle: '',
      animSpeed: 'medium',
      easingEffect: null,
      indentChildren: false,
      childrenIndenter: '&nbsp;',
    });

    //Home Slider
    $('.home-slider .slick-slider').slick({
      autoplay: true,
      autoplaySpeed: 6000,
      draggable: false,
      fade: false,
      speed: 1000,
      arrows: true,
    });

    // Home News Slider
    $('#news-slider').slick({
      autoplay: true,
      autoplaySpeed: 6000,
      dots: true,
    });

    // Resources Member section Slider
    $('#slick-slider-resources').slick({
      autoplay: true,
      autoplaySpeed: 6000,
      dots: true,
    });

    /// Add class to .banner when  scrolling to hide some elements
    var s = $('.banner');
    var pos = s.position();
    $(window).scroll(function() {
      var windowpos = $(window).scrollTop();
      if (windowpos >= pos.top) {
        s.addClass('stick');
      } else {
        s.removeClass('stick');
      }
    });

    //Exit button
    
    function leaveQuickly() {
      // Hide body
      jQuery('body').hide();

      // Get away right now
      window.open('https://weather.com', '_newtab');
      
      // Replace current site with another benign site
      window.location.replace('https://google.com');
    }

    jQuery('#exit, #exit-phone').on('click', function() {
      leaveQuickly();
    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
