export default {
  init() {
    // JavaScript to be fired on the members page
  },
  finalize() {
    // JavaScript to be fired on the members page, after the init JS
    jQuery('#nav-tab-content h3').first().ready(function() {
      var hash = window.location.hash;
      if (hash.length > 0) {
        document.querySelector(`[data-hash-id="${hash}"]`).click();
      }
    });

    // The anchors aren't being added for some reason. Gotta figure how to add them.
    jQuery('.member-area-container a.nav-link').click((event) => {
      window.location.hash = event.target.closest('a').dataset.hashId;

      // Update the form actions to include the hash in the url.
      jQuery('form').each(function( key, form ) {
        var pattern = new RegExp('gform_');
        var isGForm = form.id.match(pattern);
        if (isGForm) {
          form.action = window.location.href;
        }
      });
    });


  },
};
