import $ from 'jquery';

// wp.customize('blogname', (value) => {
//   value.bind(to => $('.brand').text(to));
// });

$(document).ready(function() {
  $('.js-accordion-toggle-switch').each(function() {
    addKeyboardClickHandlers(this);
  });

  let hamburgerMenu = $('.menu-collapser .collapse-button');
  hamburgerMenu.attr('tabindex', '0');
  hamburgerMenu.attr('aria-label', 'Open the menu');
  hamburgerMenu.attr('aria-expanded', false);
  hamburgerMenu.attr('aria-controls', 'menu-primary-navigation');
  hamburgerMenu.each(function() {
    addKeyboardClickHandlers(this);
  });
  let subMenuTogglers = $('.sub-menu + .sub-toggle');
  subMenuTogglers.each(function(index) {
    let $elem = $(this);
    $elem.attr('tabindex', '0');
    $elem.attr('id', `sub-toggle-${index}`);
    let $subMenu = $elem.closest('li').find('.sub-menu');
    $subMenu.attr('id', `sub-menu-${index}`);
    $subMenu.attr('aria-labelledby', `sub-toggle-${index}`);
    $elem.attr('aria-label', 'Open sub menu');
    $elem.attr('aria-expanded', false);
    $elem.attr('aria-controls', `sub-menu-${index}`);
    addKeyboardClickHandlers(this, $subMenu);
  });
});

function addKeyboardClickHandlers(element, moveFocusToThisElement) {
  let $elem = $(element);

  $elem.keypress(function(event) {
    if (event.which == 13 || event.keyCode == 13) {
      // Toggle control when keyboard user clicks enter
      this.click();

      // Toggle the aria-expanded property
      let expanded = $elem.attr('aria-expanded');
      expanded = (expanded === 'true') ? true : false;
      $elem.attr('aria-expanded', !expanded);

      if (moveFocusToThisElement && $elem.attr('aria-expanded') === 'true') {
        moveFocusToThisElement.find('a').first().focus();
      }
    }

  });
}
