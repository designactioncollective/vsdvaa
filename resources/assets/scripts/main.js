// import external dependencies
import 'jquery';
import 'slick-carousel/slick/slick.min.js';

// import then needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';
import {
  faArrowRight,
  faLongArrowDown,
  faTimesHexagon,
  faLongArrowRight,
} from '@fortawesome/pro-regular-svg-icons';

import {
  faPlus,
  faMinus,
} from '@fortawesome/pro-light-svg-icons';


// Import everything from autoload
import './autoload/**/*';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import members from './routes/members';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  members,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

// add the imported icons to the library
// library.add(fal, far);
library.add(
  faArrowRight,
  faLongArrowDown,
  faTimesHexagon,
  faTimesHexagon,
  faLongArrowRight,
  faPlus,
  faMinus
);

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();
