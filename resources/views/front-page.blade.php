@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <section class="home-slider">
      <?php
        //Slideshow
          echo do_shortcode('[slideshow id="247"]' );
      ?>
  </section>

  <section class="home-section blurb">
    <div class="container">

        <?php echo get_field('blurb')?>

        <div class="button-group">
          <?php if( have_rows('blurb_button') ): ?>
              <?php while( have_rows('blurb_button') ): the_row(); ?>
                  <div class="button-group__btn button-group__btn--small"> <a class="<?php echo get_sub_field('button_type')?>" href="<?php echo get_sub_field('link')?>"><?php echo get_sub_field('button_text') ?> <span class="far fa-long-arrow-right"></span></a></div>
              <?php endwhile; ?>
          <?php endif; ?>
        </div>

    </div>
  </section>

  <section class="home-section changing-culture large-container">
    <?php

      // check if the repeater field has rows of data
      if( have_rows('section_three') ):

        // loop through the rows of data
        $index = 0;
        while ( have_rows('section_three') ) : the_row();

              // display a sub field value
              $post_object = get_sub_field('page');

              if( $post_object ):
                //print_r($post_object);
                  $id = $post_object->ID;
                  $index++;
                  if( $index == 1 ) {
                    echo '<div class="">'; ?>

                  <div class="subpage-link first">
                    <a title="Read more about <?php echo get_the_title($id); ?>" href="<?php echo get_the_permalink($id); ?>">
                      <div class="subpage-link__image">
                        <?php
                          echo get_the_post_thumbnail($id, "full");
                        ?>
                      </div>
                      <div class="subpage-link__summary">
                        <h2 class="subpage-link__summary__title"><?php echo get_the_title($id); ?></h2>
                        <p class="subpage-link__summary__description"><?php echo get_the_excerpt($id); ?> </p>
                      </div>
                    </a>
                  </div>

                    <?php

                  }
                  elseif( $index >= 2) { if( $index == 2) { echo '<div class="">'; }
                ?>
                  <div class="subpage-link">
                    <a title="Read more about <?php echo get_the_title($id); ?>" href="<?php echo get_the_permalink($id); ?>">
                      <div class="subpage-link__image">
                        <?php
                          echo get_the_post_thumbnail($id, "small");
                        ?>
                      </div>
                      <div class="subpage-link__summary">
                        <h2 class="subpage-link__summary__title"><?php echo get_the_title($id); ?></h2>
                        <p class="subpage-link__summary__description"><?php echo get_the_excerpt($id); ?></p>
                      </div>
                    </a>
                  </div>
                <?php
                  }
                if( $index == 1 ) { echo '</div>'; }
                if( $index == 4 ) { echo '</div></div>'; }

              endif;

          endwhile;


      else :

          // no rows found

      endif;

    ?>
  </section>

  <section class="home-section events-news">
    <div class="large-container container">
      <div class="row">
        <div class="col-lg-6">

          <div class="events-news events">
            <?php
              $object = get_field('section_four');
              if( $object ):
                     $id = $object->ID;
                       ?>
                      <h2 class="events-news__title"><?php echo get_the_title($id); ?></h2>
                      <div class="">
                        <a title="Read more about <?php echo get_the_title($id); ?>" href="<?php echo get_the_permalink($id); ?>"  class="events-news__image">
                        <?php
                          echo get_the_post_thumbnail($id, "full");
                        ?>
                          <div class="events-news__summary">
                            <p><?php echo get_the_excerpt($id); ?> </p>
                          </div>
                        </a>
                      </div>

            <?php
              endif;
            ?>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="events-news news">
            <h2 class="events-news__title">{{ __('News', 'vsdvaa') }}</h2>
            <?php
              // Get all type post that can make it into News and that are featured as
              $args = array(
                'post_type' => array('post','resource','report','adv_racial_justice', 'press_release'),
                'posts_per_page' => -1,
                'meta_query' => array(
                              		array(
                              			'key' => 'featured_on_news',
                              			'compare' => '=',
                              			'value' => '1'
                              		)
                              	)
              );

              $news = new WP_Query($args);

              if( $news->have_posts() ): ?>
                <div id="news-slider">
                  <?php
                  while( $news->have_posts() ):
                    $news->the_post(); ?>
                    <div class="slider-item">
                      @include('partials.home-news-content')
                    </div>
                    <?php
                  endwhile; ?>
                </div>
                  <?php
              endif;
              wp_reset_postdata();
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>

@include('partials.get-help-section')

  <section class="home-section build-skills">
    <div class="container">
      <?php
        if( get_field("build_skills_blurb") ) { echo '<div class="section-title">'; the_field("build_skills_blurb"); echo '</div>'; }
      ?>
      <?php
          if( have_rows('build_skills') ):

              echo '<ul class="row">';

              while ( have_rows('build_skills') ) : the_row();

                  echo '<li class="col-6 col-xl-3 build-skills__option">';

                    $page = get_sub_field('page');

                    if( $page ):
                        $id = $page->ID;
                        $url = get_the_permalink($id);
                      ?>

                        <a title="Read more about <?php echo get_the_title($id); ?>" href="<?php echo $url; ?>">
                          <div class="icon" role="presentation">
                            <?php
                              $image = get_sub_field('icon');
                              if( $image ) { echo wp_get_attachment_image( $image, 'large' ); } ?>
                          </div>
                          <!-- TODO: I would maybe change these to <li>s -->
                            <h2><?php echo get_the_title($id); ?></h2>
                        </a>

                      <?php
                    endif; ?>

                    <?php
                  echo '</li>';

              endwhile;

              echo '</ul>';

          else :

              // no rows found

          endif;

      ?>
    </div>
  </section>

  @endwhile
@endsection
