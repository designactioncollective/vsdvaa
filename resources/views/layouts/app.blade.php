<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <?php if( !is_front_page() ) : ?>
      <div class="container">
    <?php endif; ?>
    <div class="wrap" role="document">
        @if (App\display_sidebar())
          <div class="content row">
            <main class="main col-lg-7">
              @yield('content')
            </main>
            <aside class="sidebar col-lg-5">
              @include('partials.sidebar')
              @yield('aside')
            </aside>
        @else
         <div class="content">
          <main class="main">
              @yield('content')
          </main>
        @endif

      </div>
    </div>
    <?php if( !is_front_page() ) : ?>
      </div>
    <?php endif; ?>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
