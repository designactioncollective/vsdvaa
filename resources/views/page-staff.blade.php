@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')

    <?php
      if( is_page('staff') ):
        // Loop through press_releases(news)
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        $args = array(
            'post_type' => 'staff',
            'posts_per_page' => -1,
            'paged' => $paged
          );
    ?>

    <div class="staffers">

      <?php
        $newquery = new WP_Query($args);
        if( $newquery->have_posts() ):
          while( $newquery->have_posts()): $newquery->the_post(); ?>
          <article class="staff-member">
            <div class="staff-member__image">
              <?php the_post_thumbnail( 'staff-portrait' ) ?>
              <!--div class="staff-member__image__link">
                <a href="<?php echo get_the_permalink(); ?>">Contact Staff <i class="fal fa-long-arrow-right"></i> </a>
              </div-->
            </div>
            <h3 class="staff-member__title"><!--a href="<?php echo get_the_permalink(); ?>"--><?php the_title(); ?><!--/a--></h3>
            <div class="staff-member__job-title"><?php the_field("job_info"); ?></div>
          </article>
          <?php
            wp_reset_postdata();

          endwhile;
        endif;
      ?>

    </div>

    <?php
      endif;
    ?>
  @endwhile
@endsection
