@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')
    @if (!post_password_required())
      @php
      $args = array(
        'post_type'      => 'page',
        'posts_per_page' => -1,
        'post_parent'    => $post->ID,
        'order'          => 'ASC',
        'orderby'        => 'menu_order'
      );

      $parent = new WP_Query( $args );

      $first = true;
      $num_memberships = 5;
      $count= 0;
      $style = '';

      @endphp

      @if ( $parent->have_posts() )
      <div class="row member-area-container">
        <nav class="col-md-4 col-lg-3">
          <div class="nav flex-column nav-pills" id="nav-tab" role="tablist" aria-orientation="vertical">
            @while ( $parent->have_posts() )
              @php
                $parent->the_post();
              @endphp

              <?php
                // Get the first num_memberships children pages to get the type of membership and only display the matching one
                $count++;
                if( $count <= $num_memberships ){
                  if( post_password_required(get_the_ID()) ) {
                    // move to the next
                    continue;
                  }
                }

              ?>

              <?php
              if( $count <= $num_memberships ) { ?>

                @php
                  $active = $first ? ' active' : '';
                  $pass_required = post_password_required(get_the_ID()) ? ' text-secondary not-allowed' : '';
                  $classes = 'nav-title nav-link'.$active.$pass_required;
                  $tab_content_id = 'tab-'.get_the_ID();
                  $nav_tab_id = $tab_content_id.'-tab';
                  $data_hash_id = "#".get_post_field( 'post_name', get_post() );
                  $aria_selected = $first ? 'true' : 'false';
                  $before = '<a class="'.$classes.'" id="'.$nav_tab_id.'" data-hash-id="' . $data_hash_id .'" data-toggle="pill" href="#'.$tab_content_id.'" role="tab" aria-controls="'.$tab_content_id.'" aria-selected="'.$aria_selected.'" style="'.$style.'"><div class="nav-title__image"></div><div class="nav-title__text">';
                  $after = '</div></a>';
                  the_title($before, $after);

                  $first = false;
                @endphp

              <?php } ?>

              <?php
              if( $count > $num_memberships ) {
                // check for page tags
                $tags = '';
                if( have_rows('tags', get_the_ID() ) ):
                  $tags = '<div class="page__tags">';
                  // loop through the rows of data
                    while ( have_rows('tags', get_the_ID()) ) : the_row();
                        // display a sub field value
                        if( get_sub_field("tag") )
                          $tags .= '<span class="tag">'. get_sub_field("tag") . '</span>';
                    endwhile;
                  $tags .= '</div>';
                endif;
                ?>

                  @php
                    $active = $first ? ' active' : '';
                    $pass_required = post_password_required(get_the_ID()) ? ' text-secondary not-allowed' : '';
                    $classes = 'nav-link'.$active.$pass_required;
                    $tab_content_id = 'tab-'.get_the_ID();
                    $nav_tab_id = $tab_content_id.'-tab';
                    $data_hash_id = "#".get_post_field( 'post_name', get_post() );
                    $aria_selected = $first ? 'true' : 'false';
                    $color = (get_field("color", get_the_ID())) ? get_field("color", get_the_ID()) : '#fff';
                    $style = 'margin-left: 5px; border-left: 5px solid ' . $color;
                    $desc = (get_field("description", get_the_ID())) ? get_field("description", get_the_ID()) : '';
                    $before = '<a class="'.$classes.'" id="'.$nav_tab_id.'" data-hash-id="' . $data_hash_id .'" data-toggle="pill" href="#'.$tab_content_id.'" role="tab" aria-controls="'.$tab_content_id.'" aria-selected="'.$aria_selected.'" style="'.$style.'"><div>'. $tags .'<i class="fas fa-angle-right"></i>';
                    $after = '</div><small class="text-secondary">'.$desc.'</small></a>';

                    if( has_post_thumbnail() ) the_post_thumbnail();
                    the_title($before, $after);

                    $first = false;
                  @endphp

              <?php } ?>
            @endwhile
          </div>
        </nav>

        @php
        $parent->rewind_posts();
        $first = true;
        $count = 0;
        @endphp

        <main class="col-md-8 col-lg-9">
          <div class="tab-content" id="nav-tab-content">
            @while ( $parent->have_posts() )
              @php
                $parent->the_post();
              @endphp

              <?php
                // Get the first num_memberships children pages to get the type of membership and only display the matching one
                $count++;
                if( $count <= $num_memberships ){
                  if( post_password_required(get_the_ID()) ) {
                    // move to the next
                    continue;
                  }

                }

              ?>
              @php
                $active = $first ? ' show active' : '';
                $classes = 'tab-pane fade'.$active;
                $tab_content_id = 'tab-'.get_the_ID();
                $nav_tab_id = $tab_content_id.'-tab';
                $before = '<div class="'.$classes.'" id="'.$tab_content_id.'" role="tabpanel" aria-labelledby="'.$nav_tab_id.'">';
                $after = '</div>';
              @endphp

              {!! $before !!}
              @if (post_password_required(get_the_ID()))
                <div class="alert alert-warning">Your membership tier does not grant you access to this content.</div>
              @else

                @php the_content() @endphp

                <?php
                  // Events & Actions
                  $id = get_the_ID();
                  if ($first == true) : ?>
                     @include('partials.members/events')
                     @include('partials.members/actions')
                  <?php
                  endif;

                ?>
                <?php
                  // Resources
                  $resources_id = 196;
                  if( $id == $resources_id ): ?>
                    @include('partials.members/resources')
                <?php
                  endif;
                ?>
                <?php
                  // Jobs
                  $jobs_id = 516; // 495 local 516 live
                  if($id == $jobs_id ): ?>
                    @include('partials.members/jobs')
                    <div id="job-form">
                      <?php
                      echo do_shortcode('[gravityform id=3 title=false description=false]'); // form id = 2 for local
                      ?>
                    </div>
                <?php
                  endif;
                ?>
                <?php
                  // Bulletin board
                  $bulletin_id = 190; // 536 local 190 live
                  if($id == $bulletin_id ): ?>
                    @include('partials.members/bulletin')
                    <div id="announcement">
                      <?php
                      echo do_shortcode('[gravityform id=4 title=false description=false]'); // form id = 3 for local
                      ?>
                    </div>
                <?php
                  endif;
                ?>

                <?php //if it is the first one, get the general content for the membership ?>
              @endif
              {!! $after !!}

              @php $first = false; @endphp
            @endwhile
          </div>
        </main>
      </div>
      @endif

      @php
        wp_reset_postdata();
      @endphp

    @else
    </div>
    </div>
    @endif
  @endwhile
@endsection
