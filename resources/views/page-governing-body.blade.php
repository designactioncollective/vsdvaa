@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')

    <?php the_post_thumbnail('large', array('class' => 'governing-body__featured-image'));  ?>


    <div class="governing-body__officers">
      <h2 class="governing-body__officers__title">Officers:</h2>

      <div class="governing-body__officers__list">
        <div class="row">
        <?php
          if( is_page('governing-body') ):
          $args = array(
              'post_type' => 'governing',
              'posts_per_page' => -1
            );

            $newquery = new WP_Query($args);
            if( $newquery->have_posts() ):
              while( $newquery->have_posts()): $newquery->the_post(); ?>
                  @include('partials.card-governing-body-officer')
              <?php
              endwhile;
              ?>

        </div>

        <hr class="governing-body__officers__list__hr" />

        <div class="row">

        <?php
            while( $newquery->have_posts()): $newquery->the_post(); ?>
                @include('partials.card-governing-body')
            <?php
            endwhile;
          endif;
          ?>

        </div>
      </div>
    </div>

      <?php
        wp_reset_postdata();

      endif;
    ?>
  @endwhile
@endsection
