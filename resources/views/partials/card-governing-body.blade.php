{{-- Governing Body Card--}}
<?php if( get_field('gb_titles') == '' ) { ?>
  <div class="col-xs-1 col-sm-4">
    <div class="staff-card">
      <div class="staff-card__name"><?php the_title(); ?></div>
      <hr>
      <div class="staff-card__organization"><?php if( get_field('from_the_organization') ) the_field('from_the_organization'); ?></div>
    </div>
  </div>
<?php } ?>
