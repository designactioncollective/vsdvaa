{{-- Section Events for Home page --}}
<div class="events">
            <h3>{{ __('Events', 'vsdvaa') }}</h3>
              <?php
                // Get ID for the General Events term.
                $category = get_term_by('name', 'General Events', 'tribe_events_cat');
                $term_id = $category->term_id;

                echo term_description($term_id);

                // Grab the 3 next "general" events and featured
                $events = tribe_get_events( [
                   'eventDisplay'   => 'custom',
                   'start_date'     => 'now',
                   'posts_per_page' => 3,
                   //'featured'       => true,
                   'tax_query' => array(
                                    array(
                                        'taxonomy' => 'tribe_events_cat',
                                        'field'    => 'term_id',
                                        'terms'    => $term_id,
                                    ),
                                ),
                ] );

                // Loop through the events, displaying the title for each
                foreach ( $events as $event ) {
                   echo '<h4>' . $event->post_title . '</h4>';
                }
              ?>
          </div>
