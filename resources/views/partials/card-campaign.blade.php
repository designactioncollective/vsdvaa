{{-- Campaign Card--}}
<div class="campaign-card row">
	<div class="col-sm-4 col-md-3">
		<?php
			if( has_post_thumbnail() ) the_post_thumbnail();
		?>
	</div>
	<div class="col-sm-8 col-md-9">
		<h5><a href="<?php echo get_the_permalink();?>"><?php the_title(); ?></a></h5>
		<div class="campaign-content"><?php the_advanced_excerpt(); ?></div>
	</div>
</div>
