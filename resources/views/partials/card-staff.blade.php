{{-- Staff Card--}}
<div class="staff-card">
<?php
	if( has_post_thumbnail() ) the_post_thumbnail();
?>
	<div class="name"><?php the_title(); ?></div>
	<div class="job-info"><?php if( get_field('job_info') ) the_field('job_info'); ?></div>
</div>
