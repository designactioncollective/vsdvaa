@php if ( !is_front_page() && !is_page('get-help-ayuda') ) { @endphp
  @include('partials.get-help-section-interior')
@php } else if( is_page('get-help-ayuda') ) { @endphp
  @include('partials.get-help-section')
@php } @endphp

<?php if( is_front_page() ) : ?>
  <div class="footer__get-updates-wrapper bg-purple">
    <div class="container">
      <h3><?php _e('In the Know: Get Updates','vsdvaa'); ?></h3>
    <?php
      echo do_shortcode('[gravityform id="5" title="false" description="false"]');
    ?>
    </div>
  </div>
  <!--div id="31b0cc8d-869a-4783-9598-3cedb39c13bd">
  <script type="text/javascript" src="https://default.salsalabs.org/api/widget/template/11dac939-357b-4653-bdd0-cb529ec80997/?tId=31b0cc8d-869a-4783-9598-3cedb39c13bd" ></script>
  </div-->
<?php endif; ?>
<footer class="content-info">
  <div class="container footer-widget flex-parent">
    @php dynamic_sidebar('sidebar-footer') @endphp
  </div>
  <div class="container footer-bottom">
    <div class="flex-parent">
      <!-- <div class=""> -->
        <?php
          $args = array(
            'theme_location' => 'footer_navigation-blurb',
            'menu_class' => 'main-footer-menu',
            'depth' => 1,
          );
          wp_nav_menu( $args );
        ?>
      <!-- </div> -->
      <ul class="list-inline social-media-buttons">
        <li class="list-inline-item"><a title="Follow us on Facebook" href="https://www.facebook.com/VActionAlliance/" target="_blank" class="fb"><i title="Facebook icon" class="fab fa-facebook-f"></i></a></li>
        <li class="list-inline-item"><a title="Follow us on Twitter" href="https://twitter.com/VActionAlliance" target="_blank" class="tw"><i title="Twitter icon" class="fab fa-twitter"></i></a></li>
        <li class="list-inline-item"><a title="Follow our blog" href="https://allianceinaction.org/" target="_blank" class="blog"><i title="Blogger icon" class="fas fa-blog"></i></a></li>
        <li class="list-inline-item"><a title="Follow our YouTube channel" href="https://www.youtube.com/channel/UCyifjk9sFigR_CteR8LCLTg" target="_blank" class="youtube"><i title="Youtube icon" class="fab fa-youtube"></i></a></li>
        <li class="list-inline-item"><a title="Member login" href="<?php echo get_site_url();?>/members"><i title="Icon of a key" class="fas fa-key"></i> {{ __('Member Login','vsdvaa') }}</a></li>
      </ul>
    </div>
    <div class="footer-menu">
      <div class="info">
      &copy;<?php echo date("Y"); ?> {{ __('The Virginia Sexual & Domestic Violence Action Alliance','vsdvaa') }}</div>

      {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'footer-nav']) !!}
    </div>
  </div>
  <?php ?>
</footer>
