{{--Share This--}}
<?php
  $title = get_the_title( get_the_ID() );
?>
<ul class="share-buttons">
  <li class="share-title"><span><?php _e('Share this','acre'); ?></span></li>

  <li>
    <a href="https://www.facebook.com/sharer/sharer.php" title="Share on Facebook" target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&quote=' + encodeURIComponent(document.URL)); return false;">
      <i class="fab fa-facebook-f"></i>
    </a>
  </li>

  <li>
    <a href="https://twitter.com/intent/tweet?source=https" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ':%20'  + encodeURIComponent(document.URL)); return false;">
      <i class="fab fa-twitter"></i>
    </a>
  </li>

  <li>
    <a href="mailto:?subject=&body=:%20https" target="_blank" title="Send email" onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' +  encodeURIComponent(document.URL)); return false;" >
        <i class="fas fa-envelope"></i>
    </a>
  </li>

</ul>
