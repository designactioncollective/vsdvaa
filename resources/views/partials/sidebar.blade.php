@php dynamic_sidebar('sidebar-primary') @endphp
  <?php
  // Check for custom fields for the Sidebar
  if( get_field('sidebar_button_title') ): ?>
     <div class="sidebar-box position-relative" >
       <a href="{{ get_field('link') }}">
       <button class="sidebar-box__header" >
         <div class="sidebar-box__header__question-mark">
           <img src="/wp-content/themes/vsdvaa/resources/assets/images/question-mark.png" alt="">
         </div>
         <div>
           <div class="sidebar-box__header__title">{{ get_field('sidebar_button_title') }}</div>
           <div class="sidebar-box__header__subtitle">{{ get_field('sidebar_button_subtitle') }}</div>
         </div>
         <img class="sidebar-box__header__arrow" src="/wp-content/themes/vsdvaa/resources/assets/images/arrow-right.png" alt="">
        </button></a>
       <div class="sidebar-box__content"><?php echo get_field('box_content'); ?></div>
     </div>
  <?php
  endif;
?>
