 <?php
	 $homeid =  get_option( 'page_on_front' );
 ?>
  <section class="home-section get-help">
   <div class="get-help__header container-fluid">
     <div class="large-container row">
      <div class="section-top col-xs-12 col-md-8 col-lg-9">
        <h3 class="get-help__header__title">{{ __('Find Support Near You', 'vsdvaa') }}</h3>
        <p class="get-help__header__subtitle">{{ __('Virginia Sexual & Domestic Violence Agencies', 'vsdvaa') }}</p>
      </div>
      <div class="locality col-xs-12 col-md-4 col-lg-3"><?php echo do_shortcode('[searchandfilter id="273"]'); ?></div>
     </div>
   </div>

    <div class="row">
      <div class="col-lg-8 get-help__map">
      <?php echo do_shortcode('[searchandfilter id="273" show="results"]'); ?>
      </div>
      <div class="col-lg-4 get-help__footer">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" id="get-help-tab" data-toggle="tab" href="#get-help" role="tab" aria-controls="get-help" aria-selected="true">{{ __('Get Help','vsdvaa') }}</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" id="ayuda-tab" data-toggle="tab" href="#ayuda" role="tab" aria-controls="ayuda" aria-selected="false">{{ __('Ayuda','vsdvaa') }}</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="get-help" role="tabpanel" aria-labelledby="get-help-tab">
            <?php
              if( get_field("get_help", $homeid) ) the_field("get_help", $homeid);
            ?>

            <div class="button-group">
              <?php if( have_rows('get_help_btns', $homeid) ): ?>
                  <?php while( have_rows('get_help_btns', $homeid) ): the_row(); ?>
                      <div class="button-group__btn"> <a class="<?php echo get_sub_field('button_type', $homeid)?>" href="<?php echo get_sub_field('link', $homeid)?>"><?php echo get_sub_field('button_text', $homeid)?></a></div>
                  <?php endwhile; ?>
              <?php endif; ?>
            </div>

          </div>
          <div class="tab-pane fade" id="ayuda" role="tabpanel" aria-labelledby="ayuda-tab">
            <?php
              if( get_field("ayuda", $homeid) ) the_field("ayuda", $homeid );
            ?>

            <div class="button-group">
              <?php if( have_rows('ayuda_btns', $homeid) ): ?>
                  <?php while( have_rows('ayuda_btns', $homeid) ): the_row(); ?>
                      <div class="button-group__btn"> <a class="<?php echo get_sub_field('tipo_de_boton', $homeid)?>" href="<?php echo get_sub_field('vinculo', $homeid)?>"><?php echo get_sub_field('texto_de_boton', $homeid)?></a></div>
                  <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
