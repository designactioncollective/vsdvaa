<header class="banner">

  <nav class="nav-primary">

    <div class="nav-primary__extra container">
      <div class="nav-primary__extra__logo">
        <?php
            if ( function_exists( 'the_custom_logo' ) ) {
              the_custom_logo();
            }
            ?>
      </div>

      <div class="nav-primary__extra__social-media-links d-none d-xl-inline-block">
        <ul class="list-inline">
            <li class="list-inline-item"><a title="Member login" href="<?php echo get_site_url();?>/members"><i title="Icon of a key" class="fas fa-key"></i> {{ __('Member Login','vsdvaa') }}</a></li>
            <li class="list-inline-item"><a title="Follow us on Facebook" href="https://www.facebook.com/VActionAlliance/" class="fb" target="_blank"><i title="Facebook icon" class="fab fa-facebook-f"></i></a></li>
            <li class="list-inline-item"><a title="Follow us on Twitter" href="https://twitter.com/VActionAlliance" target="_blank" class="tw"><i title="Twitter icon" class="fab fa-twitter"></i></a></li>
            <li class="list-inline-item"><a title="Follow our blog" href="https://allianceinaction.org/" target="_blank" class="blog"><i title="Blogger icon" class="fas fa-blog"></i></a></li>
            <li class="list-inline-item"><a title="Follow our YouTube channel" href="https://www.youtube.com/channel/UCyifjk9sFigR_CteR8LCLTg" target="_blank" class="youtube"><i title="Youtube icon" class="fab fa-youtube"></i></a></li>
        </ul>
      </div>
      <div class="nav-primary__extra__calls-to-action">
        <div class="nav-primary__extra__calls-to-action__first-group">
            <div class="nav-primary__extra__calls-to-action__first-group__join"><a href="<?php echo get_site_url(); ?>/become-a-member" class="btn btn-outline-primary btn-tertiary">{{ __('Join','vsdvaa') }}</a></div>
            <div class="nav-primary__extra__calls-to-action__first-group__events"><a href="<?php echo get_site_url(); ?>/events/" class="btn btn-outline-primary btn-tertiary">{{ __('Upcoming Events','vsdvaa') }}</a></div>
          </div>
        <div class="nav-primary__extra__calls-to-action__second-group">
            <div class="nav-primary__extra__calls-to-action__second-group__donate"><a href="<?php echo get_site_url(); ?>/support-our-work/donate/" class="btn btn-outline-primary btn-primary">{{ __('Donate','vsdvaa') }}</a></div>
            <div class="nav-primary__extra__calls-to-action__second-group__hamburger-menu d-inline-block d-xl-none">
              <!-- This block makes space for the hamburger menu. Ideally the hamburger HTML wouldn't be tied to to list
              (ie the hamburger menu is in one location on small screens but the ul appears somewhere else on large screens.) -->
            </div>
          </div>
      </div>
    </div>

    <div class="nav-primary__sitemap">
      <div class="container">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'slimmenu']) !!}
        @endif
      </div>
    </div>

  </nav>

  <div id="exit-phone" class=""><i class="far fa-times-hexagon"></i><a title="Use this link to quickly navigate to a new page." aria-label="Use this link to quickly navigate to a new page." href="#" rel="noreferrer">{{ _e('SAFE EXIT','vsdvaa') }}</a></div>
  <div id="exit" class="d-none d-lg-block"><i class="far fa-times-hexagon"></i><a title="Use this link to quickly navigate to a new page." aria-label="Use this link to quickly navigate to a new page." href="#" rel="noreferrer">{{ _e('SAFE EXIT','vsdvaa') }}</a></div>
</header>

