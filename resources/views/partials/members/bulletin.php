<?php
/***
	Bulletin Board
***/

  $args = array(
    'post_type' => 'post',
    'category_name' => 'member-announcements',
    'posts_per_page' => -1
  );

  $bulletin = new WP_Query($args);

  if( $bulletin->have_posts() ): ?>
    <div class="d-flex justify-content-between tab-pane__header">
      <h3><?php _e('Member Announcements/ Bulletin Board', 'vsdvaa'); ?></h3>
      <a class="btn btn-secondary" href="#announcement"><?php _e('Post an announcement','vsdvaa'); ?></a>
    </div>
    <?php
        echo do_shortcode('[searchandfilter id="5141"]'); //731  for local
        echo do_shortcode('[searchandfilter id="5141" show="results"]');
    ?>
  <?php
  endif;
  wp_reset_postdata();
?>
