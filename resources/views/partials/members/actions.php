<?php
/****
	** Actions CPT
***/
?>

  <div class="d-flex justify-content-between tab-pane__header">
  	<h3><?php _e('Actions Alerts & Announcements', 'vsdvaa'); ?></h3>
  	<a class="btn btn-secondary" href="<?php echo get_site_url(); ?>/action"><?php _e('View All', 'vsdvaa'); ?><span class="sr-only">Actions and announcements</span></a>
  </div>

<?php
  $args = array(
    'post_type' => array('action-post'),
    'posts_per_page' => 4
  );

  $actions  = new WP_Query($args);
  if( $actions->have_posts() ):
    while( $actions->have_posts() ):
      $actions->the_post();
      echo '<date>'.get_the_date().'</date>';
      echo '<a href="'. get_the_permalink() .'"><h4>'. get_the_title() . '</h4></a>';
    endwhile;
  endif;

?>

