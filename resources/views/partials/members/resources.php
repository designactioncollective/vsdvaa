<?php
/***
*** Resources for members.
*/
?>

	<?php
    $page_title = get_the_title();
    $args = array(
      'post_type' => array('resource'),
      'posts_per_page' => -1,
       'meta_query' => array(
                          array(
                              'key'   => 'featured',
                              'value' => '1',
                          )
                      )
    );

    $resources = new WP_Query($args);
    $exclude = array();

    if( $resources->have_posts() ): ?>
      <div id="slick-slider-resources">
          <?php
          while( $resources->have_posts() ):
            $resources->the_post(); ?>
            <div>
              <?php the_post_thumbnail("thumb");  ?>
                <a href="<?php echo get_the_permalink(); ?>">
                  <?php  the_title('<h4>','</h4>'); ?>
                </a>
            </div>
            <?php
            $exclude[] = get_the_ID();
          endwhile; ?>
      </div>
      <?php
    else:
      echo 'no featured resources';
    endif;

    $args = array(
      'post_type' => array('resource'),
      'posts_per_page' => -1,
      'post__not_in' => $exclude
    );


     $resources = new WP_Query($args);

      if( $resources->have_posts() ): ?>
        <div class="d-flex justify-content-between tab-pane__header">
        	<h3><?php echo $page_title; ?></h3>
        	<a class="btn btn-secondary" href="<?php echo get_site_url(); ?>/resources" target="_blank"><?php _e('View All','vsdvaa'); ?></a>
        </div>
      <ul class="resources_list">
          <?php
          while( $resources->have_posts() ):
            $resources->the_post(); ?>
            <li>
                <a href="<?php echo get_the_permalink(); ?>">
                  <?php  the_title('<h4>','</h4>'); ?>
                </a>
            </li>
            <?php
          endwhile; ?>
      </ul>
      <?php
    else:
      echo 'no resources';
    endif;



    //print_r($exclude);
	?>
