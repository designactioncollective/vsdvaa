<?php
/***
	List of Jobs
***/

$args = array(
  'post_type' => 'post',
  'category_name' => 'members-jobs',
  'posts_per_page' => -1
);

$jobs = new WP_Query($args);

if( $jobs->have_posts() ): ?>
  <div class="d-flex justify-content-between tab-pane__header">
    <h3>{{ __('Jobs', 'vsdvaa') }}</h3>
    <a class="btn btn-secondary" href="#job-form"><?php _e('Post a Job','vsdvaa'); ?></a>
  </div>
  <?php
  while($jobs->have_posts()): $jobs->the_post();
    // filter out old job posts. Compare current date with publish date and the expiration time.
    $job_id = get_the_ID();
    ?>
      <div id="accordion-block_<?php echo $job_id; ?>" class="accordion-block">
        <div class="card">
          <div class="card-header" id="heading-block_<?php echo $job_id; ?>">
            <button type="button" class="btn btn-link d-flex justify-content-between collapsed js-accordion-toggle-switch add-focus-ring" data-toggle="collapse" data-target="#collapse-block_<?php echo $job_id; ?>" aria-expanded="false" aria-controls="collapse-block_<?php echo $job_id; ?>"" aria-label="Open accordion to read content about <?php the_title(); ?>">
              <h5 class="mb-0">
                <?php the_title(); ?>
                  <br/><?php if( get_field("location") ); the_field("location"); ?>
              </h5>
              <span class="plus initial" role="presentation"><i class="fal fa-plus fa-3x"></i></span>
              <span class="minus" role="presentation"><i class="fal fa-minus fa-3x"></i></span>
            </button>
          </div>
        </div>
        <div id="collapse-block_<?php echo $job_id; ?>" class="collapse" aria-labelledby="heading-block_<?php echo $job_id; ?>" data-parent="#accordion-block_<?php echo $job_id; ?>" style="">
          <div class="card-body">
            <p>Description: <?php if( get_field("description") ); the_field("description"); ?></p>
            <?php
              if( get_field("expires_on") ) {
                    $days =  get_field("expires_on");
                    if( $days == '60 days') $days = 60;
                    else $days = 30;
                    $date_published = get_the_date();
                    //compare the date published and the number fo days to see in how many days it expires
                    $now = date("M d, Y");
                    $your_date = $date_published;

                    $datetime1 = new DateTime($now);

                    $datetime2 = new DateTime($your_date);

                    $outfor = $datetime1->diff($datetime2);

                    $daysoutfor = $outfor->days;

                    if ($daysoutfor > $days ):

                      echo '<p>Expired</p>';

                    else:
                      $remaining_days = $days - $daysoutfor;
                      echo '<p>Will expire in '. $remaining_days . ' days</p>';
                    endif;

                  }
              ?>
            <p>Organization: <?php if( get_field("organization") ) the_field("organization"); ?></p>
            <p>How to apply: <?php if( get_field("how_to_apply") ) the_field("how_to_apply"); ?></p>
            <p>Compensation: <?php if( get_field("compensation") ) the_field("compensation"); ?></p>
            <p>Who to contact: <?php if( get_field("who_to_contact") ); the_field("who_to_contact"); ?></p>
              <p>Deadline to apply: <?php if( get_field("deadline_to_apply") ); the_field("deadline_to_apply"); ?></p>

          </div>
        </div>
      </div>
  <?php
  endwhile;

endif;
wp_reset_postdata();
?>
