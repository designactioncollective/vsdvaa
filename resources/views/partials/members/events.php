<?php
/***
*** Events for members.
*/
?>
  <div class="d-flex justify-content-between tab-pane__header">
  	<h3><?php _e('Training and Events', 'vsdvaa'); ?></h3>
  	<a class="btn btn-secondary" href="<?php echo get_site_url();?>/events/category/general-events/"><?php _e('All Events', 'vsdvaa'); ?></a>
  </div>

	<?php

    // Retrieve the next 4 upcoming events
    $events = tribe_get_events( [
       'posts_per_page' => 4,
       'start_date'     => 'now',
       'tax_query'=> array(
				array(
					'taxonomy' => 'tribe_events_cat',
					'field' => 'slug',
					'terms' => 'general-events'
				)
			)
    ] );


  	if( !empty($events) ):
      foreach( $events as $event):
          $date = date("F j, Y", strtotime($event->event_date));
          echo '<date>' . $date . '</date>';
          echo '<h4><a href="' . $event->guid . '">' . $event->post_title . '</a></h4>';
          echo '<hr>';
      endforeach;
  	else:
  	  echo 'No events';
  	endif;

	?>
