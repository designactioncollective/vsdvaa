<div class="page-header">
  <?php if( the_breadcrumb() ) the_breadcrumb(); ?>
  <h1>{!! App::title() !!}</h1>
  @include('partials.share-this')
</div>
