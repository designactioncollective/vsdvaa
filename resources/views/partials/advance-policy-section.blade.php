 {{-- Advance Policy Home Section --}}
 
  <section class="home-section advance-policy">
    <div class="container">
      <div class="section-title">
        <h3>{{ __('Advance Policy', 'vsdvaa') }}</h3>
      </div>
      <div class="row">
        <?php

        // check if the repeater field has rows of data
        if( have_rows('advance_policy') ):

         	// loop through the rows of data
            while ( have_rows('advance_policy') ) : the_row();

                // display a sub field value
                 $page = get_sub_field('page');

                    if( $page ):
                        $id = $page->ID;
                        $url = get_the_permalink($id);
                      ?>
                      <div class="col-sm-6 col-lg">
                        <a href="<?php echo $url; ?>">
                          <img src="<?php echo get_the_post_thumbnail_url($id); ?>">
                        </a>
                        <h4><a href="<?php echo $url; ?>"><?php echo get_the_title($id); ?></a></h4>
                        <p class="excerpt"><?php echo get_the_excerpt($id); ?></p>
                      </div>
                      <?php
                    endif;

            endwhile;

        endif;
        ?>
      </div>
    </div>
  </section>
