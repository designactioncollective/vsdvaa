<article @php post_class() @endphp>
    <header>
      <div class="left-blur"></div>
        {{ the_post_thumbnail("large") }}
      <div class="right-blur"></div>
    </header>
    <div class="entry-summary">
        @include('partials/entry-meta')
        <h2 class="entry-title"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
        <?php the_advanced_excerpt(); ?>
    </div>
</article>
