{{--
  Title: Accordion with Image
  Description: Accordion Element
  Category: formatting
  Icon: images-alt
  Keywords: Accordion with image BS4
  Mode: edit
  Align: left
  SupportsAlign: left right
  SupportsMode: false
  SupportsMultiple: true
--}}


<div id="accordion-<?php echo $block['id']; ?>" class="accordion-block with-image">
  <div class="card">
    <div class="card-header" id="heading-<?php echo $block['id']; ?>">
        <button type="button" class="btn btn-link d-flex justify-content-between collapsed js-accordion-toggle-switch add-focus-ring" data-toggle="collapse" data-target="#collapse-<?php echo $block['id']; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $block['id']; ?>" aria-label="Open accordion to read content about {{ get_field('title') }}">
          <?php
            $image = get_field('image');
            $size = 'medium'; // (thumbnail, medium, large, full or custom size)
            if( $image ) {
                echo wp_get_attachment_image( $image, $size );
            }
          ?>
          <h5 class="mb-0">
              {{ get_field('title') }}
          </h5>
          <span class="plus initial" role="presentation"><i class="fal fa-plus fa-3x"></i></span>
          <span class="minus" role="presentation"><i class="fal fa-minus fa-3x"></i></span>
        </button>
    </div>
  </div>
  <div id="collapse-<?php echo $block['id']; ?>" class="collapse" aria-labelledby="heading-<?php echo $block['id']; ?>" data-parent="#accordion-<?php echo $block['id']; ?>">
    <div class="card-body">
      <?php if(get_field('content') ) the_field("content"); ?>
    </div>
  </div>
</div>
