@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')

    <?php
      if( is_page('news') ):
        // Loop through press_releases(news)
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        $args = array(
            'post_type' => 'press_release',
            'posts_per_page' => 3,
            'paged' => $paged
          );
    ?>

    <div class="articles">

      <?php
        $newquery = new WP_Query($args);
        if( $newquery->have_posts() ):
          while( $newquery->have_posts()): $newquery->the_post(); ?>
          <article class="excerpt">
            <div class="excerpt__thumbnail">
              <?php the_post_thumbnail() ?>
            </div>
            <div class="excerpt__text">
              <div class="excerpt__text__date"><?php echo get_the_date(); ?></div>
              <h3 class="excerpt__text__title"><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
              <hr class="hr--comments" />
              <div class="excerpt__text__comment-count">
                <a href=""><?php echo wp_count_comments()->approved ?> Comments</a>
              </div>
            </div>
          </article>
          <?php
            wp_reset_postdata();


          endwhile;
        endif;
      ?>

    </div>
    <?php
        $paginationArgs = array(
          'total' => $newquery->max_num_pages,
          'before_page_number' => '',
          'show_all' => false,
          'prev_next' => true,
          'prev_text' => "Previous",
          'next_text' => "Next",
        );
        ?>

        <div class="pagination">
          <?php echo paginate_links($paginationArgs); ?>
        </div>
    <?php
      endif;
    ?>
  @endwhile
@endsection

@section('aside')
  @while(have_posts()) @php the_post() @endphp
  <div class="news-sidebar">
    <div class="news-sidebar__text">
      <?php the_field("media_query_text", get_the_ID()); ?>
    </div>
    <div class="news-sidebar__image">
      <img src="<?php the_field("media_query_image", 343); ?>" alt="">
    </div>
  </div>
  @endwhile
@endsection
