@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')

    <?php
      if( is_page('advancing-racial-justice-page') ):
      $args = array(
          'post_type' => 'adv_racial_justice',
          'posts_per_page' => -1
        );

        $newquery = new WP_Query($args);
        if( $newquery->have_posts() ):
          while( $newquery->have_posts()): $newquery->the_post(); ?>
                @include('partials.card-campaign')
          <?php
          endwhile;
        endif;
        wp_reset_postdata();
      endif;
    ?>
  @endwhile
@endsection
