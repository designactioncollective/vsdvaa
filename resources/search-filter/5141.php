<?php
/**
 * Search & Filter Pro
 *
 * Results Template for Bulletin Board
 *
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 *
 * Note: these templates are not full page templates, rather
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think
 * of it as a template part
 *
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs
 * and using template tags -
 *
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	?>


	<?php
	while ($query->have_posts())
	{
		$query->the_post();

		    $name = get_field('bulletin_name');
		    $org = get_field('bulletin_organization');
		    $contact = get_field("contact_email_or_phone");
            $bulletin_id = get_the_ID();

            ?>
            <div id="accordion-block_<?php echo $bulletin_id; ?>" class="accordion-block">
                <div class="card">
                    <div class="card-header" id="heading-block_<?php echo $job_id; ?>">
                        <div class="btn btn-link d-flex justify-content-between collapsed" data-toggle="collapse" data-target="#collapse-block_<?php echo $bulletin_id; ?>" aria-expanded="false" aria-controls="collapse-block_<?php echo $bulletin_id; ?>">
                        <h5 class="mb-0">
                            <?php the_title(); ?>
                                  </h5>
                                  <span><i class="fas fa-arrow-circle-down"></i></span>
                                </div>
                              </div>
                            </div>
                            <div id="collapse-block_<?php echo $bulletin_id; ?>" class="collapse" aria-labelledby="heading-block_<?php echo $job_id; ?>" data-parent="#accordion-block_<?php echo $bulletin_id; ?>" style="">
                              <div class="card-body">
                                <div class="bulletin__img">
                                  <?php
                                    the_post_thumbnail("full");
                                  ?>
                                </div>
                                <div class="bulletin__entry-content">
                                  <?php
                                    echo '<date>published on: ' . get_the_date() . '</date>';
                                    if( $name ) echo '<p>Contact: ' . $name . '</p>';
                                    if( $org ) echo '<p>Organization: ' . $org . '</p>';
                                    if( $contact ) echo '<p>Contact at: ' . $contact . '</p>';
                                    echo get_the_content();
                                  ?>
                                </div>
                              </div>
                            </div>
                          </div>

		<?php
	}
	?>

	<?php
}
else
{
	echo "No Results Found";
}
?>
