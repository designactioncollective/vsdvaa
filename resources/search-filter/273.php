<?php
/**
 * Search & Filter Pro
 *
 * Sample Results Template
 *
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 *
 * Note: these templates are not full page templates, rather
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think
 * of it as a template part
 *
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs
 * and using template tags -
 *
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	?>

	<div id="map-canvas"><div class="acf-map">';

	<?php
	while ($query->have_posts())
	{
		$query->the_post();

		    $location = get_field('location');
            $title = '<a href="'.get_permalink().'">'.get_the_title().'</a>';
            if( !empty($location) ):
                echo '<div class="marker" data-lat="'.$location['lat'].'" data-lng="'.$location['lng'].'"><h4>'.$title.'</h4><div class="content-tooltip">'.$location['address'].'</div></div>';
            endif;


		?>

		<?php
	}
	?>

	</div></div>
	<?php
    include (locate_template('views/partials/map.blade.php'));
}
else
{
	echo "No Results Found";
}
?>
